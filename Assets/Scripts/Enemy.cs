using System;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public int maxHealth = 100;
    public int strength = 10;
    private int _currentHealth;

    private EnemyCollisionDetection _enemyCollisionDetection;
    
    private Vector3 _targetGridPosition;
    private Vector3 _playerTargetGridPosition;
    private Vector3 _currentGridPosition;
    public int cellSize = 5;
    public int moveSpeed = 10;
    
    private void Awake()
    {
        _enemyCollisionDetection = GetComponent<EnemyCollisionDetection>();
    }

    private void Start()
    {
        _currentHealth = maxHealth;
        _currentGridPosition = transform.position;
        _targetGridPosition = _currentGridPosition;
        InputManager.OnPlayerMove += UpdatePathToPlayer;
        InputManager.OnPlayerAttack += ReactToAttack;
    }

    private void OnDestroy()
    {
        InputManager.OnPlayerMove -= UpdatePathToPlayer;
        InputManager.OnPlayerAttack -= ReactToAttack;
    }

    private void ReactToAttack(Vector3 playerPosition)
    {
        if (this != null && IsAdjacent(playerPosition))
        {
            Attack(playerPosition);
        }
        else
        {
            UpdatePathToPlayer(playerPosition);
        }
    }

    private void UpdatePathToPlayer(Vector3 playerPosition)
    {
        if (this != null)
        {
            CalculateNextCell(playerPosition);
        }
    }
    
    public void TakeDamage(int damage)
    {
        _currentHealth -= damage;
        if (_currentHealth <= 0) Die();
    }

    private Vector3 SnapToGrid(Vector3 position)
    {
        var snappedX = Mathf.Round(position.x / cellSize) * cellSize;
        var snappedZ = Mathf.Round(position.z / cellSize) * cellSize;

        return new Vector3(snappedX, transform.position.y, snappedZ);
    }

    private void MoveToDestination()
    {
        _currentGridPosition = _targetGridPosition;
        var targetPosition = _targetGridPosition;
        targetPosition = SnapToGrid(targetPosition);
        
        // if (!EnemyCollisionDetection.CheckForCollisions(transform.position, targetPosition)) 
        transform.position = Vector3.MoveTowards(transform.position, targetPosition, 
            Time.fixedDeltaTime * moveSpeed);
    }
    
    private bool AtRest => (Vector3.Distance(transform.position, _targetGridPosition) < 0.05f);

    private void Die()
    {
        Destroy(gameObject);
    }

    private void FixedUpdate()
    {
        MoveToDestination();
    }

    private void Attack(Vector3 playerPosition)
    {
        var startPosition = transform.position;
        if (!Physics.Raycast(startPosition, playerPosition - startPosition, out var hit,
                Vector3.Distance(startPosition, playerPosition))) 
            return;

        var player = hit.collider.GetComponent<Player>();
        if (player != null) player.TakeDamage(strength);
    }
    
    private void CalculateNextCell(Vector3 playerPosition)
    {
        if (this == null) return;
        
        transform.LookAt(playerPosition);

        if (IsAdjacent(playerPosition)) { Attack(playerPosition); return; }

        var path = AStar.FindPath(transform.position, playerPosition);
        
        if (!AtRest) return;
        if (path == null) return;
        _targetGridPosition = SnapToGrid(path[0]);
    }

    private bool IsAdjacent(Vector3 playerPosition)
    {
        foreach (var neighbor in AStar.GetNeighbors(transform.position))
        {
            if (Math.Abs(playerPosition.x - neighbor.x) < 2.5f && Math.Abs(playerPosition.z - neighbor.z) < 2.5f)
            {
                return true;
            }
        }
        return false;
    }
    
}
