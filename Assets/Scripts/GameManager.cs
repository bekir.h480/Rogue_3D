using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static Queue<Action<Vector3>> _moveOrder;
    
    private void Awake()
    {
        _moveOrder = new Queue<Action<Vector3>>();
    }

    private void Start()
    {
        InputManager.EndTurn += NextMove;
    }
    
    private void OnDestroy()
    {
        InputManager.EndTurn -= NextMove;
    }

    public static void AddMove(Action<Vector3> move)
    {
        _moveOrder.Enqueue(move);
    }

    private static void NextMove(Vector3 playerPosition)
    {
        if (_moveOrder.Count == 0) return;
        _moveOrder.Dequeue()?.Invoke(playerPosition);
    }

    public static void EmptyMoveOrder()
    {
        _moveOrder.Clear();
    }

}
