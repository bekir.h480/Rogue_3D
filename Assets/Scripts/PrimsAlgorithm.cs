using System;
using System.Collections.Generic;
using UnityEngine;

public class PrimsAlgorithm : MonoBehaviour
{
    public static event Action<List<(Vector3, Vector3)>, List<Vector3>> PrimComplete; 
    
    private void Awake()
    {
        VertexCulling.TriangulationComplete += Prim;
    }

    private void OnDestroy()
    {
        VertexCulling.TriangulationComplete -= Prim;
    }

    private static void Prim(List<Vector3> roomCenters, List<(int, int)> edges)
    {
        
        var visited = new HashSet<Vector3>();
        var unvisited = new PriorityQueue<Vector3>();
        var vertexInfoMap = new List<(Vector3, Vector3)>();

        var startVertex = roomCenters[0];
        unvisited.Enqueue(startVertex, 0);
        
        while (unvisited.Count > 0)
        {
            var node = unvisited.Dequeue();
            if (visited.Contains(node)) continue;
            visited.Add(node);
            
            foreach (var (x, y) in edges)
            {
                if (x != roomCenters.IndexOf(node) && y != roomCenters.IndexOf(node)) continue;
                var from= (roomCenters.IndexOf(node) == x) ? x : y;
                var to= (roomCenters.IndexOf(node) != x) ? x : y;
                if (visited.Contains(roomCenters[to])) continue;
                var distance = Vector3.Distance(roomCenters[from], roomCenters[to]);
                unvisited.Enqueue(roomCenters[to], distance);
                vertexInfoMap.Add((roomCenters[from], roomCenters[to]));
            }
        }
        
        PrimComplete?.Invoke(vertexInfoMap, roomCenters);
    }
}
