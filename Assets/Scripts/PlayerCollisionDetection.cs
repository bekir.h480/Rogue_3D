using System;
using Unity.VisualScripting;
using UnityEngine;

public class PlayerCollisionDetection : MonoBehaviour
{
    private Player _player;

    private void Start()
    {
        _player = GetComponent<Player>();
    }

    public bool CheckForCollisions(Vector3 startPosition, Vector3 targetPosition)
    {
        if (Physics.Raycast(startPosition, targetPosition - startPosition, out var hit,
                Vector3.Distance(startPosition, targetPosition)))
        {
            // if (hit.collider.gameObject.CompareTag("Item"))
            // {
            //     return false;
            // }

            return true;
        }

        return false;
    }
}
