using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using Random = UnityEngine.Random;

public class ItemGenerator : MonoBehaviour
{
    private List<List<Vector3>> rooms = new();
    private AsyncOperationHandle<IList<GameObject>> prefabs;

    
    void Awake()
    {
        EnemyGenerator.EnemiesGenerated += LoadItems;
    }

    private void LoadItems(List<List<Vector3>> _rooms)
    {
        rooms = _rooms;
        
        prefabs = Addressables.LoadAssetsAsync<GameObject>("item", null);
        
        prefabs.Completed += _ =>
        {
            GenerateItems();
        };
    }
    
    private void GenerateItems()
    {
        for (var i = 0; i < 5; i++)
        {
            var room = rooms[Random.Range(0, rooms.Count)];
            var spawnLocation = room[Random.Range(0, room.Count)];
            room.Remove(spawnLocation);
            spawnLocation.y = 2.75f;
            var item = Instantiate(prefabs.Result[Random.Range(0, prefabs.Result.Count)], 
                                                spawnLocation, 
                                                Quaternion.identity);
            item.transform.SetParent(transform, true);
        }
    }
}
