using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using GameObject = UnityEngine.GameObject;
using Random = UnityEngine.Random;

public class RoomGenerator : MonoBehaviour
{
    public int maxDepth = 4;
    public int minRoomSize = 15;

    public Vector3 mapStartPoint = new Vector3(0, 2.75f, 0);
    public Vector3 mapEndPoint = new Vector3(100, 2.75f, 100);
    
    public int cellSize = 5;
    
    private BSPnode _startNode;
    
    private List<Rect> generatedRooms = new List<Rect>();
    private AsyncOperationHandle<IList<GameObject>> smallRoomsHandle;
    private AsyncOperationHandle<IList<GameObject>> mediumRoomsHandle;
    private AsyncOperationHandle<IList<GameObject>> bigRoomsHandle;
    private AsyncOperationHandle<IList<GameObject>> tallRoomsHandle;
    private AsyncOperationHandle<IList<GameObject>> floorHandle;
    
    public static event Action<List<Rect>> RoomsGenerated;
    public static event Action RoomsLoaded;

    private void Awake()
    {
        RoomsLoaded += Begin;
    }

    private void Start()
    {
        smallRoomsHandle = Addressables.LoadAssetsAsync<GameObject>("SmallRoom", null);
        mediumRoomsHandle = Addressables.LoadAssetsAsync<GameObject>("MediumRoom", null);
        bigRoomsHandle = Addressables.LoadAssetsAsync<GameObject>("BigRoom", null);
        tallRoomsHandle = Addressables.LoadAssetsAsync<GameObject>("TallRoom", null);
        floorHandle = Addressables.LoadAssetsAsync<GameObject>("floor", null);

        smallRoomsHandle.Completed += _ => OnRoomsLoaded();
        mediumRoomsHandle.Completed += _ => OnRoomsLoaded();
        bigRoomsHandle.Completed += _ => OnRoomsLoaded();
        tallRoomsHandle.Completed += _ => OnRoomsLoaded();
        floorHandle.Completed += _ => OnRoomsLoaded();
    }

    private void OnRoomsLoaded()
    {
        if (smallRoomsHandle.IsDone && mediumRoomsHandle.IsDone && bigRoomsHandle.IsDone && tallRoomsHandle.IsDone 
            && floorHandle.IsDone)
        {
            RoomsLoaded?.Invoke();
        }
    }

    private void OnDestroy()
    {
        RoomsLoaded -= Begin;
    }

    private void Begin()
    {
        GenerateFloor(mapStartPoint, mapEndPoint);
        
        mapStartPoint.x *= cellSize;
        mapStartPoint.z *= cellSize;
        mapEndPoint.x *= cellSize;
        mapEndPoint.z *= cellSize;
        var curatedRect = new Rect(mapStartPoint.x, mapStartPoint.z, mapEndPoint.x, mapEndPoint.z);
        _startNode = Generate_BSP_Tree(maxDepth, curatedRect);
        
        GenerateRooms(_startNode);
        RoomsGenerated?.Invoke(generatedRooms);
    }

    private BSPnode Generate_BSP_Tree(int depth, Rect partition)
    {
        var rectangles = SplitRect(partition);
        if (depth == 0 || rectangles[0] == Rect.zero)
        {
            return new BSPnode
            {
                Partition = partition,
                LeftChild = null,
                RightChild = null
            };
        }

        var node = new BSPnode
        {
            Partition = partition,
            LeftChild = new BSPnode(),
            RightChild = new BSPnode()
        };
        
        node.LeftChild = Generate_BSP_Tree(depth - 1, rectangles[0]);
        node.RightChild = Generate_BSP_Tree(depth - 1, rectangles[1]);
        
        return node;
    }

    private void GenerateRooms(BSPnode node)
    {
        if (node == null) return;
        
        if (node.IsLeaf()) GenerateRoom(node);
        else
        {
            GenerateRooms(node.LeftChild);
            GenerateRooms(node.RightChild);
        }
    }

    private void GenerateRoom(BSPnode node)
    {
        if (IsRoomOverlap(node.Partition)) { return; }
        
        var center = node.Partition.center;
        var buildPosition = new Vector3(center.x, 0, center.y);
        buildPosition = SnapToGrid(buildPosition);
        var width = node.Partition.width;
        var height = node.Partition.height;
        var room = GetRoomBySize(width, height);
        var roomInstance = Instantiate(room, buildPosition, 
            (width > height) ? Quaternion.Euler(0, 90, 0) : Quaternion.identity);
        roomInstance.transform.SetParent(transform, true);
        
        generatedRooms.Add(node.Partition);
    }

    private GameObject GetRoomBySize(float width, float height)
    {
        // if (Math.Abs(width - 200) < 1 || Math.Abs(height - 200) < 1) return tallRoomsHandle.Result[Random.Range(0,tallRoomsHandle.Result.Count)];
        // if (Math.Abs(width - 100) < 1 && Math.Abs(height - 100) < 1) return bigRoomsHandle.Result[Random.Range(0,bigRoomsHandle.Result.Count)];
        if (Math.Abs(width - 100) < 1 || Math.Abs(height - 100) < 1) return mediumRoomsHandle.Result[Random.Range(0,mediumRoomsHandle.Result.Count)];
        return smallRoomsHandle.Result[Random.Range(0,smallRoomsHandle.Result.Count)];
    }

    private Vector3 GetRoomCenter(int x, int y, int width, int height)
    {
        var centerX = x + width / 2f;
        var centerY = y + height / 2f;
        return new Vector3(centerX, 2.75f, centerY);
    }
    
    private Vector3 SnapToGrid(Vector3 position)
    {
        var snappedX = Mathf.Round(position.x / cellSize) * cellSize;
        var snappedZ = Mathf.Round(position.z / cellSize) * cellSize;

        return new Vector3(snappedX, position.y, snappedZ);
    }

    private Rect[] SplitRect(Rect rect)
    {
        var splitRects = new Rect[2];

        var splitAlongWidth = Random.value > 0.5f;
        
        if (splitAlongWidth)
        {
            // var splitX = (int)Math.Round((Random.Range(rect.xMin + minRoomSize, rect.width - minRoomSize)/2)*2);
            var splitX = (int)Math.Floor(rect.width / 2);
            if (splitX - rect.xMin < minRoomSize || rect.height < minRoomSize || rect.width - splitX < minRoomSize)
            {
                splitRects[0] = Rect.zero;
                splitRects[1] = Rect.zero;
                return splitRects;
            }
            splitRects[0] = new Rect(rect.xMin, rect.yMin, splitX - rect.xMin, rect.height);
            splitRects[1] = new Rect(splitX, rect.yMin, rect.width - splitX, rect.height);
        }
        else
        {
            // var splitY = (int)Math.Round((Random.Range(rect.yMin + minRoomSize, rect.height - minRoomSize)/2)*2);
            var splitY = (int)Math.Floor(rect.height / 2);
            if (splitY - rect.yMin < minRoomSize || rect.width < minRoomSize || rect.height - splitY < minRoomSize)
            {
                splitRects[0] = Rect.zero;
                splitRects[1] = Rect.zero;
                return splitRects;
            }
            splitRects[0] = new Rect(rect.xMin, rect.yMin, rect.width, splitY - rect.yMin);
            splitRects[1] = new Rect(rect.xMin, splitY, rect.width, rect.height - splitY);
        }

        return splitRects;
    }
    
    private bool IsRoomOverlap(Rect room)
    {
        return generatedRooms.Any(existingRoom => RectOverlaps(room, existingRoom));
    }
    
    private bool RectOverlaps(Rect rect1, Rect rect2)
    {
        return rect1.xMin < rect2.width &&
               rect1.width > rect2.xMin &&
               rect1.yMin < rect2.height &&
               rect1.height > rect2.yMin;
    }

    private void GenerateFloor(Vector3 start, Vector3 end)
    {
        var x = start.x;
        var z = start.z;
        var width = end.x;
        var height = end.z;

        for (var i = 0; i <= width; i++)
        {
            for (var j = 0; j <= height; j++)
            {
                var floor = Instantiate(floorHandle.Result[0], 
                    new Vector3(i * cellSize + x, -0.25f, j * cellSize + z), 
                    Quaternion.identity);
                floor.transform.SetParent(transform, true);
            }
        }
    }
}

public class BSPnode
{
    public BSPnode LeftChild;
    public BSPnode RightChild;
    public Rect Partition;

    public bool IsLeaf()
    {
        return LeftChild == null && RightChild == null;
    }
}