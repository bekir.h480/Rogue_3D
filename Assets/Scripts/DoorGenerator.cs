using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class DoorGenerator : MonoBehaviour
{
    public GameObject archwayPrefab;
    private static int CellSize = 5;
    
    private void Start()
    {
        HallwayGenerator.HallwayGenerationComplete += GenerateDoors;
    }
    private void OnDestroy()
    {
        HallwayGenerator.HallwayGenerationComplete -= GenerateDoors;
    }

    private void GenerateDoors(List<List<Vector3>> rooms)
    {
        foreach (var room in rooms)
        {
            foreach (var cell in room)
            {
                foreach (var neighbor in AStarForMap.GetNeighbors(cell))
                {
                    if (!HallwayGenerator.IsInRoom(neighbor, room) && IsInBetweenWalls(neighbor))
                    {
                        var door = Instantiate(archwayPrefab, neighbor,
                            (Math.Abs(neighbor.x - cell.x) > 0.1f) 
                                ? Quaternion.Euler(0, 90, 0) 
                                : Quaternion.identity);
                        door.transform.SetParent(transform, true);
                    }
                }   
            }
        }
    }
    
    private bool IsInBetweenWalls(Vector3 position)
    {
        return !HallwayGenerator.IsWall(position) &&
               ((HallwayGenerator.IsWall(new Vector3(position.x + CellSize, position.y, position.z)) &&
                 HallwayGenerator.IsWall(new Vector3(position.x - CellSize, position.y, position.z))) ||
                (HallwayGenerator.IsWall(new Vector3(position.x, position.y, position.z + CellSize)) &&
                 HallwayGenerator.IsWall(new Vector3(position.x, position.y, position.z - CellSize))));
    }
}
