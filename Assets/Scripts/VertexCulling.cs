using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Quaternion = UnityEngine.Quaternion;
using Random = UnityEngine.Random;
using Vector3 = UnityEngine.Vector3;
using Vector2 = UnityEngine.Vector2;

public class VertexCulling : MonoBehaviour
{
    private List<Room> rooms = new List<Room>();
    private List<Vector3> roomCenters = new List<Vector3>();
    private List<Vector2> roomCentersV2 = new List<Vector2>();
    public GameObject slimePrefab;
    public GameObject markPrefab;
    public int cellSize = 5;
    private List<Vector3> _dfsVisited = new();

    public static event Action<List<Vector3>, List<(int,int)>> TriangulationComplete; 
    
    private void Test(List<Rect> generatedRooms)
    {
        foreach (var room in generatedRooms)
        {
            var newRoom = new Room
            {
                x = (int)room.x + cellSize,
                y = (int)room.y + cellSize,
                width = (int)room.width - 2*cellSize,
                height = (int)room.height - 2*cellSize
            };
            rooms.Add(newRoom);
        }

        foreach (var room in rooms)
        { 
            var roomCenter = GetRoomCenter(room);
            roomCenter = SnapToGrid(roomCenter);
            roomCenters.Add(roomCenter);
            roomCenter.y = 2.75f;
            // Instantiate(slimePrefab, roomCenter, Quaternion.identity);
        }

        foreach (var room in rooms)
        {
            _dfsVisited.Clear();
            Dfs(GetRoomCenter(room));
            room.space = _dfsVisited;
        }

        var randomlyChosenPoints = new List<Vector3>();
        foreach (var room in rooms)
        {
            randomlyChosenPoints.Add(room.space[Random.Range(0, room.space.Count)]);
        }
        
        // roomCentersV2 = extractVector2s(roomCenters);
        // var edges = BowyerWatson.DelaunayTriangulation(roomCentersV2);
        roomCentersV2 = extractVector2s(randomlyChosenPoints);
        var edges = BowyerWatson.DelaunayTriangulation(roomCentersV2);
        TriangulationComplete?.Invoke(roomCenters, edges);
    }

    private void Dfs(Vector3 cell)
    {
        if (_dfsVisited.Contains(cell) || HallwayGenerator.IsWall(cell)) return;
        
        _dfsVisited.Add(cell);
        
        Dfs(cell + new Vector3(1, 0, 0) * cellSize);
        Dfs(cell + new Vector3(-1, 0, 0) * cellSize);
        Dfs(cell + new Vector3(0, 0, 1) * cellSize);
        Dfs(cell + new Vector3(0, 0, -1) * cellSize);
    }

    private List<Vector2> extractVector2s(List<Vector3> vector3s)
    {
        return vector3s.Select(vector3 => new Vector2(vector3.x, vector3.z)).ToList();
    }
    
    private Vector3 GetRoomCenter(Room room)
    {
        var centerX = room.x + room.width / 2f;
        var centerY = room.y + room.height / 2f;
        return new Vector3(centerX, 0, centerY); // -----------2.75f-----------0f----------------
    }
    
    private Vector3 SnapToGrid(Vector3 position)
    {
        var snappedX = Mathf.Round(position.x / cellSize) * cellSize;
        var snappedZ = Mathf.Round(position.z / cellSize) * cellSize;

        return new Vector3(snappedX, position.y, snappedZ);
    }
     
    private void Awake()
    {
        RoomGenerator.RoomsGenerated += Test;
    }

    private void OnDestroy()
    {
        RoomGenerator.RoomsGenerated -= Test;
    }

}

public class Room
{ 
    public int x;
    public int y;
    public int width;
    public int height;
    public List<Vector3> space;
}
