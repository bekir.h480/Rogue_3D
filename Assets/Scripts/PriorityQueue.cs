using System.Collections.Generic;
using System.Linq;

public class PriorityQueue<T>
{
    private readonly SortedDictionary<float, Queue<T>> _dict = new SortedDictionary<float, Queue<T>>();

    public int Count
    {
        get
        {
            return _dict.Values.Sum(queue => queue.Count);
        }
    }

    public void Enqueue(T item, float priority)
    {
        if (!_dict.ContainsKey(priority))
            _dict[priority] = new Queue<T>();
        _dict[priority].Enqueue(item);
    }

    public T Dequeue()
    {
        var queue = _dict[_dict.Keys.First()];
        var item = queue.Dequeue();
        if (queue.Count == 0)
            _dict.Remove(_dict.Keys.First());
        return item;
    }

    public bool Contains(T item)
    {
        return _dict.Values.Any(queue => queue.Contains(item));
    }
}