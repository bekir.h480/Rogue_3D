using System;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using Random = UnityEngine.Random;

public class EnemyGenerator : MonoBehaviour
{
    private List<List<Vector3>> rooms = new();
    private AsyncOperationHandle<IList<GameObject>> prefabs;
    public static event Action<List<List<Vector3>>> EnemiesGenerated; 

    
    void Awake()
    {
        PlayerGenerator.PlayerGenerated += LoadEnemies;
    }
    
    void OnDestroy()
    {
        PlayerGenerator.PlayerGenerated -= LoadEnemies;
    }

    private void LoadEnemies(List<List<Vector3>> _rooms)
    {
        rooms = _rooms;
        
        prefabs = Addressables.LoadAssetsAsync<GameObject>("enemy", null);
        
        prefabs.Completed += _ =>
        {
            GenerateEnemies();
        };
    }

    private void GenerateEnemies()
    {
        for (var i = 0; i < 5; i++)
        {
            var room = rooms[Random.Range(0, rooms.Count)];
            var spawnLocation = room[Random.Range(0, room.Count)];
            room.Remove(spawnLocation);
            spawnLocation.y = 2.75f;
            var enemy = Instantiate(prefabs.Result[Random.Range(0, prefabs.Result.Count)], 
                                                spawnLocation, 
                                                Quaternion.identity);
            enemy.transform.SetParent(transform, true);
        }
        EnemiesGenerated?.Invoke(rooms);
    }
}
