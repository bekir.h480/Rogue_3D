using UnityEngine;

public class PlayerLook : MonoBehaviour
{
    public Camera mainCamera;
    private float _xRotation;

    public float xSensitivity = 30f;
    public float ySensitivity = 30f;
    
    public void ProcessLook(Vector2 mouseInput)
    {
        _xRotation -= (mouseInput.y * Time.fixedDeltaTime) * ySensitivity;
        _xRotation = Mathf.Clamp(_xRotation, -80f, 80f);

        mainCamera.transform.localRotation = Quaternion.Euler(_xRotation, 0, 0);
        
        transform.Rotate(Vector3.up * ((mouseInput.x * Time.fixedDeltaTime) * xSensitivity));
    }
}
