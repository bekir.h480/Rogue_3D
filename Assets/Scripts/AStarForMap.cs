using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AStarForMap : MonoBehaviour
{

    private const int CellSize = 5;

    private struct CellInfo
    {
        public Vector3 parent; 
        public float f; 
        public float g; 
        public float h;
    }

    public static List<Vector3> FindPath(Vector3 start, Vector3 goal)
    {
        var closedList = new HashSet<Vector3>();
        var cellInfoMap = new Dictionary<Vector3, CellInfo>();
        
        var startCell = new CellInfo
        {
            parent = start,
            g = 0,
            h = Heuristic(start, goal)
        };
        startCell.f = startCell.g + startCell.h;
        cellInfoMap[start] = startCell;
        
        var openList = new PriorityQueue<Vector3>();
        openList.Enqueue(start, startCell.f);
        
        while (openList.Count > 0)
        {
            var current = openList.Dequeue();
            
            closedList.Add(current);

            foreach (var neighbor in GetNeighbors(current))
            {
                if (neighbor == goal)
                {
                    var path = new List<Vector3>();
                    while (current != start)
                    {
                        path.Add(current);
                        current = cellInfoMap[current].parent;
                    }
                    path.Reverse();
                    return path;
                }
                if (closedList.Contains(neighbor))
                    continue;
                
                var tentativeG = cellInfoMap[current].g + Vector3.Distance(current, neighbor);
                if (!cellInfoMap.ContainsKey(neighbor) || tentativeG < cellInfoMap[neighbor].g)
                {
                    CellInfo neighborCell = new CellInfo();
                    neighborCell.parent = current;
                    neighborCell.g = tentativeG;
                    neighborCell.h = Heuristic(neighbor, goal);
                    neighborCell.f = neighborCell.g + neighborCell.h;
                    cellInfoMap[neighbor] = neighborCell;


                    if (!openList.Contains(neighbor))
                        openList.Enqueue(neighbor, neighborCell.f);
                }
            }
        }

        // No path found
        return null;
    }

    public static IEnumerable<Vector3> GetNeighbors(Vector3 cell)
    {
        yield return cell + new Vector3(1, 0, 0) * CellSize;
        yield return cell + new Vector3(-1, 0, 0) * CellSize; 
        yield return cell + new Vector3(0, 0, 1) * CellSize; 
        yield return cell + new Vector3(0, 0, -1) * CellSize; 
        // yield return cell + new Vector3(1, 0, 1) * CellSize; 
        // yield return cell + new Vector3(-1, 0, 1) * CellSize; 
        // yield return cell + new Vector3(1, 0, -1) * CellSize; 
        // yield return cell + new Vector3(-1, 0, -1) * CellSize; 
    }
    
    private static float Heuristic(Vector3 startCell, Vector3 endCell)
    {
        var distance = Vector3.Distance(startCell, endCell);
    
        var directionChangePenalty = CalculateDirectionChangePenalty(startCell, endCell);
        
        return distance + directionChangePenalty;
    }

    private static float CalculateDirectionChangePenalty(Vector3 startCell, Vector3 endCell)
    {
        var direction = (endCell - startCell).normalized;
    
        var dotProduct = Vector3.Dot(direction, Vector3.forward);
    
        var angle = Mathf.Acos(dotProduct) * Mathf.Rad2Deg;
    
        var penalty = angle * 3;
    
        return penalty;
    }

}

