using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BowyerWatson : MonoBehaviour
{

    private static List<Vector2> points = new ();
    private static List<(int,int)> triangulation = new ();

    public static List<(int, int)> DelaunayTriangulation(List<Vector2> _points)
    {
        points = _points;

        var superMin = new Vector2(float.MaxValue, float.MaxValue);
        var superMax = new Vector2(float.MinValue, float.MinValue);
        foreach (var point in points)
        {
            if (point.x < superMin.x) superMin.x = point.x;
            if (point.y < superMin.y) superMin.y = point.y;
            if (point.x > superMax.x) superMax.x = point.x;
            if (point.y > superMax.y) superMax.y = point.y;
        }

        var dx = superMax.x - superMin.x;
        var dy = superMax.y - superMin.y;
        var deltaMax = Mathf.Max(dx, dy);
        var mid = new Vector2((superMin.x + superMax.x) / 2, (superMin.y + superMax.y) / 2);

        var p1 = new Vector2(mid.x - 2 * deltaMax, mid.y - deltaMax);
        var p2 = new Vector2(mid.x, mid.y + 2 * deltaMax);
        var p3 = new Vector2(mid.x + 2 * deltaMax, mid.y - deltaMax);

        points.Add(SnapToGrid(p1));
        points.Add(SnapToGrid(p2));
        points.Add(SnapToGrid(p3));

        var triangles = new List<Triangle>();

        triangles.Add(new Triangle(points.Count - 3, points.Count - 2, points.Count - 1));

        foreach (var point in points)
        {
            var edges = new List<Edge>();

            for (var i = triangles.Count - 1; i >= 0; i--)
            {
                var triangle = triangles[i];
                if (triangle.IsPointInsideCircumcircle(points, point))
                {
                    edges.Add(new Edge(triangle.v1, triangle.v2));
                    edges.Add(new Edge(triangle.v2, triangle.v3));
                    edges.Add(new Edge(triangle.v3, triangle.v1));
                    triangles.RemoveAt(i);
                }
            }

            foreach (var edge in edges)
            {
                if (IsRealPoint(edge.v1) && IsRealPoint(edge.v2))
                {
                    if (!IsDuplicateEdge(edge.v1, edge.v2))
                    {
                        triangulation.Add((edge.v1, edge.v2));
                    }
                }
                if (IsRealPoint(edge.v2) && IsRealPoint(points.IndexOf(point)))
                {
                    if (!IsDuplicateEdge(edge.v2, points.IndexOf(point)))
                    {
                        triangulation.Add((edge.v2, points.IndexOf(point)));
                    }
                }
                if (IsRealPoint(points.IndexOf(point)) && IsRealPoint(edge.v1))
                {
                    if (!IsDuplicateEdge(points.IndexOf(point), edge.v1))
                    {
                        triangulation.Add((points.IndexOf(point), edge.v1));
                    }
                }
                triangles.Add(new Triangle(edge.v1, edge.v2, points.IndexOf(point)));
            }
        }

        return triangulation;
    }

    private static bool IsDuplicateEdge(int v1, int v2)
    {
        foreach (var (t1, t2) in triangulation)
        {
            if ((v1 == t1 && v2 == t2) || (v1 == t2 && v2 == t1))
                return true;
        }

        return false;
    }

    private static Vector2 SnapToGrid(Vector2 position)
    {
        var cellSize = 5;
        var snappedX = Mathf.Round(position.x / cellSize) * cellSize;
        var snappedZ = Mathf.Round(position.y / cellSize) * cellSize;

        return new Vector2(snappedX, snappedZ);
    }

    private static bool IsRealPoint(int point)
    {
        return point != points.Count - 3 &&
               point != points.Count - 2 &&
               point != points.Count - 1;
    }
}

public class Triangle
{
    public int v1;
    public int v2;
    public int v3;

    public Triangle(int v1, int v2, int v3)
    {
        this.v1 = v1;
        this.v2 = v2;
        this.v3 = v3;
    }

    public bool ContainsVertex(int v)
    {
        return v1 == v || v2 == v || v3 == v;
    }

    public bool IsPointInsideCircumcircle(List<Vector2> points, Vector2 point)
    {
        var p1 = points[v1];
        var p2 = points[v2];
        var p3 = points[v3];

        var ax = p1.x - point.x;
        var ay = p1.y - point.y;
        var bx = p2.x - point.x;
        var by = p2.y - point.y;
        var cx = p3.x - point.x;
        var cy = p3.y - point.y;

        var ab = ax * (p2.y - p1.y) - ay * (p2.x - p1.x);
        var bc = bx * (p3.y - p2.y) - by * (p3.x - p2.x);
        var ca = cx * (p1.y - p3.y) - cy * (p1.x - p3.x);

        return (ab * bc > 0 && bc * ca > 0 && ca * ab > 0);
    }
}

public class Edge
{
    public int v1;
    public int v2;

    public Edge(int v1, int v2)
    {
        this.v1 = v1;
        this.v2 = v2;
    }

    public override bool Equals(object obj)
    {
        if (obj == null || GetType() != obj.GetType()) return false;
        var other = (Edge)obj;
        return (v1 == other.v1 && v2 == other.v2) || (v1 == other.v2 && v2 == other.v1);
    }

    public override int GetHashCode()
    {
        return v1.GetHashCode() ^ v2.GetHashCode();
    }
}

