
using System;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public int maxHealth = 100;
    public int strength = 10;
    
    private int _currentHealth;
    private static List<Item> _inventory = new();

    private void Start()
    {
        _currentHealth = maxHealth;
    }

    public void TakeDamage(int damage)
    {
        _currentHealth -= damage;
        if (_currentHealth <= 0) Die();
    }

    public static void PickUp(Item item)
    {
        _inventory.Add(item);
        Debug.Log("Picked up: " + item);
    }

    private void Die()
    {
        Destroy(gameObject);
    }
}
