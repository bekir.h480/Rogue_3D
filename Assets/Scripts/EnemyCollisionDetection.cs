using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCollisionDetection : MonoBehaviour
{
    private Enemy _enemy;

    private void Start()
    {
        _enemy = GetComponent<Enemy>();
    }

    public static bool CheckForCollisions(Vector3 startPosition, Vector3 targetPosition)
    {
        return Physics.Raycast(startPosition, targetPosition - startPosition, out var hit,
            Vector3.Distance(startPosition, targetPosition));
    }
}
