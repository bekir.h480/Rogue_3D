using System;
using System.Collections;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;

public class InputManager : MonoBehaviour
{
    private PlayerInput _playerInput;
    private PlayerLook _playerLook;
    private Player _player;
    private PlayerCollisionDetection _playerCollisionDetection;
    
    public int moveSpeed = 10;
    public int cellSize = 5;
    
    private Vector3 _targetGridPosition;
    private Vector3 _currentGridPosition;

    public static event Action<Vector3> OnPlayerMove; 
    public static event Action<Vector3> OnPlayerAttack; 
    public static event Action<Vector3> EndTurn; 
    
    private void Awake()
    {
        _playerLook = GetComponent<PlayerLook>();
        _playerCollisionDetection = GetComponent<PlayerCollisionDetection>();
        _player = GetComponent<Player>();
        _playerInput = new PlayerInput();
        _playerInput.Normal.Movement.performed += MovePlayer;
        _currentGridPosition = transform.position;
        _targetGridPosition = _currentGridPosition;
    }

    private void MovePlayer(InputAction.CallbackContext context)
    {
        var moveInput = context.ReadValue<Vector3>();
        if (moveInput.x != 0 && moveInput.z != 0)
        { 
            moveInput.x = (moveInput.x > 0) ? 1 : -1;
            moveInput.z = (moveInput.z > 0) ? 1 : -1;
        }

        var playerTransform = transform;
        var moveDirection = (playerTransform.forward * moveInput.z) + (playerTransform.right * moveInput.x);

        var targetPosition = _targetGridPosition + (moveDirection * cellSize);

        if (!AtRest) return;
        if (_playerCollisionDetection.CheckForCollisions(transform.position, targetPosition))
        { Attack(targetPosition); EndTurn?.Invoke(playerTransform.position); return; }
        
        _targetGridPosition = SnapToGrid(targetPosition);
        GameManager.AddMove(OnPlayerMove);
    }

    public void Attack(Vector3 enemyPosition)
    {
        var startPosition = transform.position;
        if (!Physics.Raycast(startPosition, enemyPosition - startPosition, out var hit,
                Vector3.Distance(startPosition, enemyPosition))) 
            return;

        var enemy = hit.collider.GetComponent<Enemy>();
        
        if (enemy == null) return;
        enemy.TakeDamage(_player.strength);
        GameManager.AddMove(OnPlayerAttack);
    }

    private Vector3 SnapToGrid(Vector3 position)
    {
        var snappedX = Mathf.Round(position.x / cellSize) * cellSize;
        var snappedZ = Mathf.Round(position.z / cellSize) * cellSize;

        return new Vector3(snappedX, transform.position.y, snappedZ);
    }

    private void MoveToDestination()
    {
        var lastPosition = transform.position;
        _currentGridPosition = _targetGridPosition;
        var targetPosition = _targetGridPosition;
        
        var newPosition =
            Vector3.MoveTowards(lastPosition, targetPosition,
                Time.fixedDeltaTime * moveSpeed);
        transform.position = newPosition;
        if (AtRest && lastPosition != newPosition)
            EndTurn?.Invoke(newPosition);
    }
    
    private bool AtRest => (Vector3.Distance(transform.position, _targetGridPosition) < 0.05f);

    private void FixedUpdate()
    {
        MoveToDestination();
    }

    private void LateUpdate()
    {
        _playerLook.ProcessLook(_playerInput.Normal.Look.ReadValue<Vector2>());
    }

    private void OnEnable()
    {
        _playerInput?.Enable();
    }

    private void OnDisable()
    {
        _playerInput?.Disable();
    }
}