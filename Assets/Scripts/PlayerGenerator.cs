using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using Random = UnityEngine.Random;

public class PlayerGenerator : MonoBehaviour
{
    public static event Action<List<List<Vector3>>> PlayerGenerated;
    private List<List<Vector3>> _rooms = new();
    private AsyncOperationHandle<IList<GameObject>> prefabs;
    
    private void Awake()
    {
        HallwayGenerator.HallwayGenerationComplete += LoadPlayer;
    }
    private void OnDestroy()
    {
        HallwayGenerator.HallwayGenerationComplete -= LoadPlayer;
    }

    private void LoadPlayer(List<List<Vector3>> rooms)
    {
        _rooms = rooms;
        
        prefabs = Addressables.LoadAssetsAsync<GameObject>("player", null);
        
        prefabs.Completed += _ =>
        {
            GeneratePlayer();
        };
    }

    private void GeneratePlayer()
    {
        var startingRoom = _rooms[0];
        var spawnLocation = startingRoom[Random.Range(0, startingRoom.Count)];
        startingRoom.Remove(spawnLocation);
        spawnLocation.y = 2.75f;
        Instantiate(prefabs.Result[0], spawnLocation, Quaternion.identity);
        PlayerGenerated?.Invoke(_rooms);
    }
}
