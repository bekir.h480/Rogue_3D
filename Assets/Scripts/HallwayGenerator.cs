using System;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;

public class HallwayGenerator : MonoBehaviour
{
    public GameObject markPrefab;
    public GameObject mark2Prefab;
    public GameObject pillarWallPrefab;
    public GameObject archwayPrefab;
    private List<Vector3> _dfsVisited = new();
    private List<Vector3> _startRoom = new();
    private List<Vector3> _endRoom = new();
    private List<List<Vector3>> rooms = new();
    private List<List<Vector3>> hallways = new();
    private List<(Vector3, Vector3)> edges = new();
    private static int CellSize = 5;
    public static event Action<List<List<Vector3>>> HallwayGenerationComplete; 

    private void Awake()
    {
        PrimsAlgorithm.PrimComplete += GenerateHallways;
    }

    private void OnDestroy()
    {
        PrimsAlgorithm.PrimComplete -= GenerateHallways;
    }

    private void GenerateHallways(List<(Vector3, Vector3)> _edges, List<Vector3> roomCenters)
    {
        edges = _edges;
        
        foreach (var roomCenter in roomCenters)
        {
            if (!IsInEdges(roomCenter)) edges.Add((roomCenter, NearestPoint(roomCenter, roomCenters)));
            _dfsVisited.Clear();
            Dfs(roomCenter);
            rooms.Add(new List<Vector3>(_dfsVisited));
        }
        
        foreach (var (v1, v2) in edges)
        {
            var path= AStarForMap.FindPath(v1, v2);
            
            _startRoom = GetRoom(v1);
            _endRoom = GetRoom(v2);
            
            var hallway = new List<Vector3>();
            var isHallway = false;
            foreach (var cell in path)
            {
                if (IsInRoom(cell, _endRoom))
                {
                    break;
                }
                if (!IsInRoom(cell, _startRoom) && !hallway.Contains(cell))
                {
                    hallway.Add(cell);
                    isHallway = true;
                }
                if (isHallway && !hallway.Contains(cell)) hallway.Add(cell);
            }
            
            for (var i = 0; i < hallway.Count-1; i++)
            {
                foreach (var neighbor in AStar.GetNeighbors(hallway[i]))
                {
                    if (!IsWall(neighbor) && 
                        !IsInAnyRoom(neighbor))
                    {
                        var hallwayBlock = Instantiate(pillarWallPrefab, neighbor, Quaternion.identity);
                        hallwayBlock.transform.SetParent(transform, true);
                    }
                }
            }
            hallways.Add(hallway);
        }

        foreach (var hallway in hallways)
        {
            foreach (var cell in hallway)
            {
                DestroyIfWall(cell);
            }
        }
        
        HallwayGenerationComplete?.Invoke(rooms);
    }

    
    private bool IsInEdges(Vector3 point)   
    {
        foreach (var (v1, v2) in edges)
        {
            if (v1 == point || v2 == point) return true;
        }

        return false;
    }

    public static bool IsInRoom(Vector3 cell, List<Vector3> room)
    {
        return room.Contains(cell);
    }
    
    private bool IsInAnyRoom(Vector3 cell)
    {
        foreach (var room in rooms)
        {
            if (room.Contains(cell)) return true;
        }

        return false;
    }

    private void Dfs(Vector3 cell)
    {
        if (_dfsVisited.Contains(cell) || IsWall(cell)) return;
        
        _dfsVisited.Add(cell);
        
        Dfs(cell + new Vector3(1, 0, 0) * CellSize);
        Dfs(cell + new Vector3(-1, 0, 0) * CellSize);
        Dfs(cell + new Vector3(0, 0, 1) * CellSize);
        Dfs(cell + new Vector3(0, 0, -1) * CellSize);
    }

    private void DestroyIfWall(Vector3 position)
    {
        var startPosition = new Vector3(position.x, 10, position.z);
        var direction = Vector3.down;
        var ray = new Ray(startPosition, direction);
        Physics.Raycast(ray, out var hit);
        var obj = hit.collider;
        if (obj != null)
        {
            var gameObj = hit.collider.gameObject;
            if (gameObj.activeSelf)
            {
                if (gameObj.CompareTag("Obstacle"))
                {
                    gameObj.SetActive(false);
                    Destroy(gameObj);                    
                }
            }
        }
    }

    public static bool IsWall(Vector3 position)
    {
        var startPosition = new Vector3(position.x, 10, position.z);
        var direction = Vector3.down;
        var ray = new Ray(startPosition, direction);
        RaycastHit hit;
        Physics.Raycast(ray, out hit);
        if (hit.collider != null) 
        {
            if (hit.collider.gameObject.activeSelf)
            {
                return hit.collider.gameObject.CompareTag("Obstacle");    
            }
        }
        return false;
    }
    

    private List<Vector3> GetRoom(Vector3 center)
    {
        foreach (var room in rooms)
        {
            if (IsInRoom(center, room)) return room;
        }
        return new List<Vector3>();
    }

    private Vector3 NearestPoint(Vector3 start, List<Vector3> points)
    {
        var distance = float.MaxValue;
        var end = new Vector3();
        foreach (var point in points)
        {
            if (Vector3.Distance(start, point) < distance && start != point && IsInEdges(point))
            {
                distance = Vector3.Distance(start, point);
                end = point;
            }
        }
        return end;
    }
}
